package com.hamersoft.passwordmanager;

import android.content.Context;
import android.security.KeyPairGeneratorSpec;
import android.util.Base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.security.auth.x500.X500Principal;

public class KeyStoreAccess {

    private final String androidKeyStore = "AndroidKeyStore";
    private final String keyStoreAccess = "KeyStoreAccess";
    private String alias;
    private KeyStore keyStore;
    private Context context;

    public KeyStoreAccess(Context context, String alias) {
        this.context = context;
        this.alias = alias;
        createKeyStore();
        createKeys(alias);
    }

    public KeyStoreAccess(Context context) {
        this.context = context;
        createKeyStore();
    }

    private void createKeyStore() {
        try {
            keyStore = KeyStore.getInstance(androidKeyStore);
            keyStore.load(null);
        } catch (Exception ignored) {
        }
    }

    public List<String> getKeyAliases() {
        List<String> keyAliases = new ArrayList<>();
        try {
            Enumeration<String> aliases = keyStore.aliases();
            while (aliases.hasMoreElements()) {
                keyAliases.add(aliases.nextElement());
            }
        } catch (Exception ignored) {
        }

        return keyAliases;
    }

    public void createKeys(String alias) {
        if (Utils.isNullOrEmpty(alias))
            return;
        try {
            if (!keyStore.containsAlias(alias)) {
                Calendar start = Calendar.getInstance();
                Calendar end = Calendar.getInstance();
                end.add(Calendar.YEAR, 1);
                KeyPairGeneratorSpec spec = new KeyPairGeneratorSpec.Builder(context)
                        .setAlias(alias)
                        .setSubject(new X500Principal("CN=Sample Name, O=Android Authority"))
                        .setSerialNumber(BigInteger.ONE)
                        .setStartDate(start.getTime())
                        .setEndDate(end.getTime())
                        .build();
                KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", "AndroidKeyStore");
                generator.initialize(spec);

                KeyPair keyPair = generator.generateKeyPair();
            }
        } catch (Exception ignored) {
        }
    }

    public String encrypt(String plainText) {
        return encrypt(plainText, alias);
    }

    public String encrypt(String plainText, String alias) {
        if (Utils.isNullOrEmpty(plainText) || Utils.isNullOrEmpty(alias)) {
            return "";
        }

        try {
            KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(alias, null);
            RSAPublicKey publicKey = (RSAPublicKey) privateKeyEntry.getCertificate().getPublicKey();

            Cipher inCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding", "AndroidOpenSSL");
//            Cipher inCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            inCipher.init(Cipher.ENCRYPT_MODE, privateKeyEntry.getCertificate().getPublicKey());

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            CipherOutputStream cipherOutputStream = new CipherOutputStream(
                    outputStream, inCipher);
            cipherOutputStream.write(plainText.getBytes("UTF-8"));
            cipherOutputStream.close();

            byte[] vals = outputStream.toByteArray();
            return Base64.encodeToString(vals, Base64.DEFAULT);
        } catch (Exception ignored) {
        }
        return "";
    }

    public String decrypt(String encryptedString) {
        return decrypt(encryptedString, alias);
    }

    public String decrypt(String encryptedString, String alias) {
        if (Utils.isNullOrEmpty(encryptedString) || Utils.isNullOrEmpty(alias))
            return "";

        try {
            KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(alias, null);
            // RSAPrivateKey privateKey = (RSAPrivateKey) privateKeyEntry.getPrivateKey();
//            Cipher output = Cipher.getInstance("RSA/ECB/PKCS1Padding", "AndroidOpenSSL");
            Cipher output = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            output.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());

            CipherInputStream cipherInputStream = new CipherInputStream(
                    new ByteArrayInputStream(Base64.decode(encryptedString, Base64.DEFAULT)), output);
            ArrayList<Byte> values = new ArrayList<>();
            int nextByte;
            while ((nextByte = cipherInputStream.read()) != -1) {
                values.add((byte) nextByte);
            }

            byte[] bytes = new byte[values.size()];
            for (int i = 0; i < bytes.length; i++) {
                bytes[i] = values.get(i).byteValue();
            }

            return new String(bytes, 0, bytes.length, "UTF-8");


        } catch (Exception ignored) {
        }
        return "";
    }

    public void delete() {
        delete(alias);
    }

    public void delete(String alias) {
        if (Utils.isNullOrEmpty(alias))
            return;
        try {
            keyStore.deleteEntry(alias);
        } catch (KeyStoreException ignored) {
        }
    }

}
