package com.hamersoft.passwordmanager;

import android.app.Application;
import android.content.Context;

public final class Main extends Application {

    private Context context;
    private KeyStoreAccess keyStoreAccess;
    private PasswordManager passwordManager;
    private SharedPreferencesAccess sharedPreferencesAccess;

    public Main(Context context, String alias, String sharedPreferencesKey) {
        this.context = context;
        keyStoreAccess = new KeyStoreAccess(this.context, alias);
        passwordManager = new PasswordManager(this.context, alias, sharedPreferencesKey);
        sharedPreferencesAccess = new SharedPreferencesAccess(this.context, "Main");
    }

    public KeyStoreAccess getKeyStoreAccess() {
        return keyStoreAccess;
    }

    public PasswordManager getPasswordManager() {
        return passwordManager;
    }

    public SharedPreferencesAccess getSharedPreferencesAccess() {
        return sharedPreferencesAccess;
    }
}