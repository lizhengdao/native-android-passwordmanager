package com.hamersoft.passwordmanager;

import android.content.Context;

public class PasswordManager {

    private final KeyStoreAccess keystoreAccess;
    private final SharedPreferencesAccess sharedPreferences;
    private final String sharedPreferenceKey;

    public PasswordManager(Context context, String alias, String sharedPreferenceKey) {
        this.sharedPreferenceKey = sharedPreferenceKey;
        keystoreAccess = new KeyStoreAccess(context, alias);
        sharedPreferences = new SharedPreferencesAccess(context, alias);
    }

    public boolean savePassword(String password) {
        String encryptedPassword = keystoreAccess.encrypt(password);
        sharedPreferences.put(sharedPreferenceKey, encryptedPassword);
        return sharedPreferences.hasValue(sharedPreferenceKey);
    }

    public boolean deletePassword() {
        if (sharedPreferences.hasValue(sharedPreferenceKey))
            sharedPreferences.delete(sharedPreferenceKey);

        return !sharedPreferences.hasValue(sharedPreferenceKey);
    }

    public String getPassword() {
        if (sharedPreferences.hasValue(sharedPreferenceKey))
            return keystoreAccess.decrypt(sharedPreferences.get(sharedPreferenceKey));
        return "";
    }
}
