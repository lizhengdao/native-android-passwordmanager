package com.hamersoft.passwordmanager;

import android.content.Context;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class MainInstrumentedTest extends AbstractInstrumentedTest {

    @Test
    public void useAppContext() {
        assertEquals("com.hamersoft.passwordmanager", context.getPackageName());
    }

    @Test
    public void WhenMainIsConstructed_KeyStoreAccess_IsInitialized() {
        assertNotNull(main.getKeyStoreAccess());
    }

    @Test
    public void WhenMainIsConstructed_PasswordManager_IsInitialized() {
        assertNotNull(main.getPasswordManager());
    }

    @Test
    public void WhenMainIsConstructed_SharedReferencesAccess_IsInitialized() {
        assertNotNull(main.getSharedPreferencesAccess());
    }

}