package com.hamersoft.passwordmanager;

import android.content.Context;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class AbstractInstrumentedTest {

    protected static Context context;
    protected static Main main;
    protected final String password = "hamersoft";

    @BeforeClass
    public static void beforeClass() {
        context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        main = new Main(context, "Unit-Testing-Alias", "my-password-key");
    }



}