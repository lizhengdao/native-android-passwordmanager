package com.hamersoft.passwordmanager;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.ext.junit.runners.AndroidJUnit4;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class KeyStoreInstrumentationTest extends AbstractInstrumentedTest {

    @Test
    public void useAppContext() {
        assertEquals("com.hamersoft.passwordmanager", context.getPackageName());
    }

    @Test
    public void CreateKeystore_Creates_A_New_KeyStore_With_The_Given_Alias() {
        assertFalse(main.getKeyStoreAccess().getKeyAliases().size()>0);
    }

    @Test
    public void KeyStore_Encrypt_Returns_Cipher_Text() {
        String encryptedText = main.getKeyStoreAccess().encrypt(password);
        assertNotEquals(password, encryptedText);
        assertFalse(encryptedText.contains(password));
    }

    @Test
    public void KeyStore_Encrypt_Returns_Empty_Text_When_Called_With_Null() {
        String encryptedText = main.getKeyStoreAccess().encrypt(null);
        assertEquals("", encryptedText);
    }

    @Test
    public void KeyStore_Encrypt_Returns_Empty_Text_When_Called_With_Empty() {
        String encryptedText = main.getKeyStoreAccess().encrypt("");
        assertEquals("", encryptedText);
    }

    @Test
    public void KeyStore_Decrypt_Returns_Correct_PlainText() {
        String encryptedText = main.getKeyStoreAccess().encrypt(password);
        assertEquals(password, main.getKeyStoreAccess().decrypt(encryptedText));
    }

    @Test
    public void KeyStore_Decrypt_Returns_EmptyString_When_EncryptedText_Is_Null() {
        assertEquals("", main.getKeyStoreAccess().decrypt(null));
    }

    @Test
    public void KeyStore_Decrypt_Returns_EmptyString_When_EncryptedText_Is_Empty() {
        assertEquals("", main.getKeyStoreAccess().decrypt(""));
    }

    @Test
    public void Keystore_Delete_Removes_The_KeyStore() {
        main.getKeyStoreAccess().delete();
        assertEquals(0, main.getKeyStoreAccess().getKeyAliases().size());
    }

    @Test
    public void Keystore_Delete_Throws_No_Exception_When_Called_With_Null_Alias() {
        main.getKeyStoreAccess().delete(null);
        assertEquals(0,0);
    }
    @Test
    public void Keystore_Delete_Throws_No_Exception_When_Called_With_Empty_Alias() {
        main.getKeyStoreAccess().delete("");
        assertEquals(0,0);
    }

    @Test
    public void When_KeystoreAccess_Is_Created_Without_Alias_The_KeyStore_Is_Not_Created() {
        KeyStoreAccess ksa = new KeyStoreAccess(context);
        ksa.createKeys(null);
        ksa.createKeys("");
        assertEquals(0, 0);
    }

    public void TearDown(){
        main.getKeyStoreAccess().delete();
    }
}