# Native Android PasswordManager

This is a simple native Android library to access the KeyStore and SharedPreferences.
Together they simulate a keychain like functionality.

It Offers a Utility class [PasswordManager](app/src/main/java/com/hamersoft/passwordmanager/PasswordManager.java) for easy access.
But you can also use the [KeyStoreAccess](app/src/main/java/com/hamersoft/passwordmanager/KeyStoreAccess.java) and [SharedPreferencesAccess](app/src/main/java/com/hamersoft/passwordmanager/SharedPreferencesAccess.java) to access them directly.

# Usage & Tests

Example of usage and InstrumentationTests can be found [here](app/src/androidTest/java/com/hamersoft/passwordmanager/).